data "terraform_remote_state" "project" {
	backend = "gcs" 

	config = {
    bucket = "st-terraform-test"
    prefix = "testing/terraform.tfstate"
	}
}

data "terraform_remote_state" "red" {
	backend = "gcs" 

	config = {
    bucket = "st-terraform-test"
    prefix = "subnet/terraform.tfstate"
	}
}

provider "google" {
    project =  data.terraform_remote_state.project.outputs.project
}


resource "google_container_cluster" "gke-cluster-testing" {
    name = join("-",[var.name,"gke"])
    location = (var.region ? var.region : var.zone) ## Solo es un condicional si existe region entonces asignale region sino la zona por default

    description = var.description
    
    remove_default_node_pool = true ## El cluster si o si necesita un pool de nodos para crearse. Lo mejor es eliminar el pool default
    initial_node_count = 1

    master_auth {
        username = ""
        password = ""

        client_certificate_config {
            issue_client_certificate = false
        }
    }

    ## En algunos casos se suele buggear la SA que se crea por default para asignar a los pool de nodos
    ## En ese caso (para no tener que llegar a instancias mayores) es mejor crear una SA customizada
    ## Si bien este nodo sera efimero, si o si necesita una SA, recuerde eso
    #node_config {
    #    service_account =  
    #}

    resource_labels = var.resource_labels

    addons_config {
        horizontal_pod_autoscaling {
            disabled = false
        }

        http_load_balancing {
            disabled = false
        }
    
        ## En la version 0.12 esta en beta, por lo que no habilitaremos esta opcion
        #istio_config {
        #    disabled = false
        #}
    }
    ip_allocation_policy {
        cluster_secondary_range_name = "pods" ### Lo etiquetamos en la creacion de la subnet para gke
        services_secondary_range_name = "services"
    }

    ## Si el cluster es privado
    #private_cluster_config {
    #   enable_private_nodes = true  ## si o si debe estar en true
    #   enable_private_enpoint = true  ## Habilita que tus enpoint sea el rango que asignas en el siguiente rango
    #   master_ipv4_cidr_block = var.master_range  ## el rango debe ser /28
    #}
    #
    ## Esta seccion es donde pondremos los rangos o las ip que tendran acceso a la administracion del cluster
    ## Si nuetro cluster es privado es necesario agregar estas lineas sino no tendremos acceso al mismo
    #master_authorized_networks_config {
    #   cidr_blocks {
    #        display_name = var.display ## Es una descripcion
    #        cidr_block = var.range_access ## Rango que tendra acceso a administrar el cluster
    #    }
    #
    #    cidr_blocks {
    #        //
    #    }    
    #}

    network = data.terraform_remote_state.project.outputs.network
    subnetwork = data.terraform_remote_state.red.outputs.subnet-gke

}

resource "google_container_node_pool" "nodes-pool-testing" {
    count = length(var.machine_type)
    name = join("-",["pool",var.machine_type[count.index],count.index])
    location = (var.region ? var.region : var.zone)
    cluster = google_container_cluster.gke-cluster-testing.name

    node_count = var.node_count

    node_config {
        # preemptible = true ## Para instancias efimeras
        machine_type = var.machine_type[count.index]

        labels = var.labels

        # tags = var.tags ## Reglas de FW creadas

        ## Para habilitar el autoscaling
        #autoscaling {
        #    min_node_count = 3
        #    max_node_count = 6
        #}

        metadata = {
            disable-legacy-endpoints = "ture"
        }

        ##service_account = var.service_account

        oauth_scopes = [
            "storage-ro",
            "storage-rw",
            "logging-write",
            "monitoring"
        ]

    }

    timeouts {
        create = "30m"
        update = "40m"
    }
}