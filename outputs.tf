output "cluster" {
    value = google_container_cluster.gke-cluster-testing.endpoint
}

output "ip-node" {
    value = ["${google_container_node_pool.nodes-pool-testing.*.instance_group_urls}"]
}