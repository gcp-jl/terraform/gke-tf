variable "name" {
    type = string
    default = "test"
}

variable "machine_type" {
    type = list
    default = ["n1-standard-1"]
}

variable "region" {
    default = false
}

variable "zone" {
    type = string
    default = "us-central1-a"
}

variable "labels" {
    type = map
    default = {
        owner = "jeyseven"
		tier  = "test"
		object = "node"
    }
}
    
variable "resource_labels" {
    type = map
    default = {
        owner = "jeyseven"
        tier = "test"
        object = "cluster"
    }
}

variable "description" {
    type = string
    default = "This is a test node pool"
}

variable "node_count" {
    type = string
    default = 3
}
